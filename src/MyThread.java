//Current time was taken as milliseconds of the system
public class MyThread implements Runnable{
    private String name;
    public MyThread(String name){
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 7; i++) {
            System.out.println("[" + name + "]" + "-" + "[" + System.currentTimeMillis() + "]");
            try{
                Thread.sleep(1000);//sleep for a second
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args){
        MyThread myt1 = new MyThread("BThread1");
        MyThread myt2 = new MyThread("BThread2");
        MyThread myt3 = new MyThread("BThread3");

        Thread t1 = new Thread(myt1);
        Thread t2 = new Thread(myt2);
        Thread t3 = new Thread(myt3);

        t1.start();
        t2.start();
        t3.start();
    }
}
