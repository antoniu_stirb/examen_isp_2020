//temporary association between U and B
//composition between B and C
//direct association between B and D
//aggregation between E and B
//
public class A{}

public class B extends A{//inheritance between A and B
    private String param;
    private C c = new C();
    private D d;

    public B(){
    }

    public setD(D d){
        this.d = d;
    }

    public void x(){
    }

    public void y(){
    }
}

public class C{
}

public class D{}

public class E{
    private B b;
    public E(B b){
        this.b = b;
    }
}

public class U{
    public void doSomethingWithB(B b){}
}